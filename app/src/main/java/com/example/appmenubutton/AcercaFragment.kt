package com.example.appmenubutton

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class AcercaFragment : Fragment() {
    private lateinit var rcvLista: RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var btnNuevo: FloatingActionButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_acerca, container, false)

        rcvLista = view.findViewById(R.id.recId)
        btnNuevo = view.findViewById(R.id.agregarAlumno)
        rcvLista.layoutManager = LinearLayoutManager(requireContext())

        val listaAlumno = arrayListOf(
            AlumnoLista(1, "202003100", "LIZÁRRAGA CAMACHO JESÚS ARMANDO", "", "ING. TEC INFORMACIÓN", R.mipmap.a01),
            AlumnoLista(2, "202003101", "LIZÁRRAGA CAMACHO JESÚS ARMANDO", "", "ING. TEC INFORMACIÓN", R.mipmap.a02),
            AlumnoLista(3, "202003102", "LIZÁRRAGA CAMACHO JESÚS ARMANDO", "", "ING. TEC INFORMACIÓN", R.mipmap.a03),
            AlumnoLista(1, "202003103", "LIZÁRRAGA CAMACHO JESÚS ARMANDO", "", "ING. TEC INFORMACIÓN", R.mipmap.a04),
            AlumnoLista(2, "202003104", "LIZÁRRAGA CAMACHO JESÚS ARMANDO", "", "ING. TEC INFORMACIÓN", R.mipmap.a05),
            AlumnoLista(3, "202003105", "LIZÁRRAGA CAMACHO JESÚS ARMANDO", "", "ING. TEC INFORMACIÓN", R.mipmap.a06),
            AlumnoLista(1, "202003106", "LIZÁRRAGA CAMACHO JESÚS ARMANDO", "", "ING. TEC INFORMACIÓN", R.mipmap.a07),
            AlumnoLista(2, "202003107", "LIZÁRRAGA CAMACHO JESÚS ARMANDO", "", "ING. TEC INFORMACIÓN", R.mipmap.a08),
            AlumnoLista(3, "202003108", "LIZÁRRAGA CAMACHO JESÚS ARMANDO", "", "ING. TEC INFORMACIÓN", R.mipmap.a09),
            AlumnoLista(1, "202003109", "LIZÁRRAGA CAMACHO JESÚS ARMANDO", "", "ING. TEC INFORMACIÓN", R.mipmap.a10)
        )

        adaptador = MiAdaptador(listaAlumno, requireContext())
        rcvLista.adapter = adaptador

        btnNuevo.setOnClickListener {
            cambiarDbFragment()
        }

        adaptador.setOnClickListener { view ->
            val pos = rcvLista.getChildAdapterPosition(view)
            val alumno = listaAlumno[pos]

            val bundle = Bundle().apply {
                putSerializable("mialumno", alumno)
            }

            val dbFragment = DbFragment().apply {
                arguments = bundle
            }

            parentFragmentManager.beginTransaction()
                .replace(R.id.frmContenedor, dbFragment)
                .addToBackStack(null)
                .commit()
        }

        return view
    }

    private fun cambiarDbFragment() {
        val cambioFragment = parentFragmentManager.beginTransaction()
        cambioFragment.replace(R.id.frmContenedor, DbFragment())
        cambioFragment.addToBackStack(null)
        cambioFragment.commit()
    }
}