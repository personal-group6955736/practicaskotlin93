package com.example.appmenubutton.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

class DefinirTabla {
    //clase estatica, no genera objetos, hace refencia con el nombre de la clase y atributos

    object Alumnos : BaseColumns{


        const val TABLA = "alumnos"
        const val ID = "id"
        const val MATRICULA = "matricula"
        const val NOMBRE = "nombre"
        const val DOMICILIO = "domicilio"
        const val ESPECIALIDAD = "especialidad"
        const val FOTO = "foto"
    }
}
